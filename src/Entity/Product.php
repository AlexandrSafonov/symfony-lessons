<?php

namespace App\Entity;

use App\Entity\Size;

class Product
{
    
    private $id;
    
    private $name;
    
    private $description;
    
    private $sizes;
    
    private $reviews;
    
    private $user;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id) 
    {
        $this->id = $id;
    }
    
    public function getName() 
    {
        return $this->name;
    }
    
    public function setName($name) 
    {
        $this->name = $name;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    public function setDescription($description) 
    {
        $this->description = $description;
    }
    
    public function getSizes()
    {
        return $this->sizes;
    }
    
    public function setSizes($sizes)
    {
        $this->sizes = $sizes;
    }
    
    public function addSize(Size $size)
    {
        $this->sizes[] = $size;
    }    

    public function getReviews()
    {
        return $this->reviews;
    }
    
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    }
    
    public function addReviews($review)
    {
        $this->reviews[] = $review;
    }     
    
    public function getUser() 
    {
        return $this->user;
    }
    
    public function setUser($user)
    {
        $this->user = $user;
    }
    
    public function __toString() 
    {
        return $this->name;
    }
}
