<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;

class User extends BaseUser
{
    protected $id;
    
    protected $first_name;

    protected $last_name;

    protected $reviews;
    
    protected $products;
    
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
    }
 
    public function getFirstName()
    {
        return $this->first_name;
    }
    
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
    }

    public function getLastName()
    {
        return $this->last_name;
    }

    public function getReviews()
    {
        return $this->reviews;
    }
    
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    }
    
    public function getProducts()
    {
        return $this->products;
    }
    
    public function setProducts($products)
    {
        $this->products = $products;
    }

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
}
