<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class Size
{
    
    private $id;
    
    private $name;

    private $products;
    
    private $reviews;
    
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }
    
    public function getId() 
    {
        return $this->id;
    }
    
    public function setId($id) 
    {
        $this->id = $id;
    }
    
    public function getName() 
    {
        return $this->name;
    }
    
    public function setName($name) 
    {
        $this->name = $name;
    }

    public function getProducts()
    {
        return $this->products;
    }
    
    public function getReviews()
    {
        return $this->reviews;
    }
    
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
    }
    
    public function __toString() 
    {
        return $this->name;
    }
}
