<?php

namespace App\Entity;

class Review
{
    private $id;
    
    private $product;

    private $message;
    
    private $user;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function getProduct()
    {
        return $this->product;
    }
    
    public function setProduct($product)
    {
        $this->product = $product;
    }
    
    public function getMessage()
    {
        return $this->message;
    }
    
    public function setMessage($message)
    {
        $this->message = $message;
    }
    
    public function getUser()
    {
        return $this->user;
    }
    
    public function setUser($user) 
    {
        $this->user = $user;
    }
    
    public function __toString() 
    {
        return 'Review id: '.$this->id;
    }
}
