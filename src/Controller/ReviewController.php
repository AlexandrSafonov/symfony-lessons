<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Review;
use App\Form\ReviewFormType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class ReviewController extends Controller {

    /**
     * @Route("/review/create", name="create_product_review")
     */
    public function createAction(
        Request $request, EntityManagerInterface $entityManager
    ) {
        $review = new Review();
        $form = $this->createForm(ReviewFormType::class, $review);
        $form->handleRequest($request);

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $review->setUser($user);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($review);
            $entityManager->flush();

        }

        return $this->redirectToRoute(
            'product_show', ['id' => $review->getProduct()->getId()]
        );
    }
    
    /**
     * @Route("/review/remove", name="remove_product_review")
     */
    public function removeAction(
        EntityManagerInterface $entityManager    
    ) {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            
        }
    }

}
