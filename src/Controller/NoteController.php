<?php

namespace App\Controller;

use App\Form\NoteFormType;
use App\Entity\Note;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class NoteController extends Controller {

    public function createAction(
        Request $request,
        EntityManagerInterface $entityManager
    ) {
        $note = new Note();
        $form = $this->createForm(NoteFormType::class, $note);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($note);
            $entityManager->flush();
        }
        
        return $this->redirect($this->generateUrl('note.list'));
    }

    public function listAction(
        Request $request,
        ValidatorInterface $validator
    ) {
        $note = new Note();
        $form = $this->createForm(NoteFormType::class, $note);
        $form->handleRequest($request);
        $errors = $validator->validate($note);
        
        $notes = $this
            ->getDoctrine()
            ->getRepository(Note::class)
            ->findAll();
        
        return $this->render('form.html.twig', [
            'form' => $form->createView(),
            'notes' => $notes,
            'errors' => $errors,
        ]);
    }

}
