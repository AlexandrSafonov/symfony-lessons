<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Review;
use App\Form\ProductFormType;
use App\Form\ReviewFormType;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class ProductController extends Controller
{
    /**
     * @Route("/products", name="products_list")
     */
    public function listAction()
    {
        $products = $this
            ->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();
        
        return $this->render('product/index.html.twig', [
            'products' => $products,
        ]);
    }
    
    
    /**
     * @Route("/product/create", name="product_create")
     */
    public function createAction(
        Request $request,
        EntityManagerInterface $entityManager
    ) {
        $product = new Product();
        
        $form = $this->createForm(ProductFormType::class, $product);
        $form->handleRequest($request);
        
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $product->setUser($user);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($product);
            $entityManager->flush();
            return $this->redirectToRoute('products_list');
        }
        
        return $this->render('product/create-page.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * @Route("/product/{id}", name="product_show")
     */
    public function showAction($id) {
        $product = $this
            ->getDoctrine()
            ->getRepository(Product::class)
            ->find($id);
        
        $reviews = $product->getReviews();
        
        return $this->render('product/single-page.html.twig', [
            'product' => $product,
            'form'    => $this->getReviewFormView($product),
            'reviews' => $reviews
        ]);
    }
    
    private function getReviewFormView($product)
    {
        $review = new Review();
        $review->setProduct($product);
        
        $form = $this->createForm(ReviewFormType::class, $review);
        
        return $form->createView();
    }
}
